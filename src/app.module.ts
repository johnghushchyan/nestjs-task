import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CryptoModule } from './crypto/crypto.module';
import { Module } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as path from 'path';
dotenv.config({ path: path.join(__dirname + '/../.env') });

console.log('{mongodb://localhost/' + process.env.MONGO_DB1)
@Module({
  imports: [
    MongooseModule.forRoot('{mongodb://localhost/' + process.env.MONGO_DB1, {
      useNewUrlParser: true,
    }),
    CryptoModule,
  ],

  controllers: [AppController],
  providers: [AppService],
})

export class AppModule {}
