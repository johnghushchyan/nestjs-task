import { Schema } from 'mongoose';

export const CryptoSchema = new Schema({
    RAW: Object,
    DISPLAY: Object,
    createdAt: { type: Date, default: Date.now },
});
