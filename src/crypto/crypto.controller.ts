import { Controller, Res, HttpStatus, Get, Query } from '@nestjs/common';
import { CryptoService } from './crypto.service';
import * as dotenv from 'dotenv';
import * as path from 'path';
dotenv.config({ path: path.join(__dirname + '/../../.env') });
import { CryptoQueryDTO } from './dto/crypto.dto';
import * as request from 'request';

@Controller('crypto')
export class CryptoController {

    constructor(private cryptoService: CryptoService) { }

    @Get('/')
    async getCrypto(@Res() res, @Query() cryptoQueryDTO: CryptoQueryDTO) {
        const {fsyms, tsyms} = cryptoQueryDTO;
        if (!fsyms || !tsyms) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                status: 400,
                created: false,
            });
        }
        const url = `${process.env.CRYPTO_PATH}?fsyms=${fsyms}&tsyms=${tsyms}`;
        const responseCrypto = await new Promise((resolve, reject) => {
            request.get(url, (error, response, body) => {
                if (error) {
                    return reject(error);
                }
                const cryptoStructureJSON = JSON.parse(body);
                return resolve(cryptoStructureJSON);
            });
        });
        const cryptoCheck = await this.cryptoService.createCrypto(responseCrypto);
        return res.status(HttpStatus.OK).json({
            status: 200,
            created: cryptoCheck,
        });
    }
}
