import { Document } from 'mongoose';

export interface Crypto extends Document {
    readonly RAW: string;
    readonly DISPLAY: string;
}
