import { CryptoService } from './crypto.service';
import { CryptoController } from './crypto.controller';
import * as redisStore from 'cache-manager-redis-store';
import { CacheModule, Module } from '@nestjs/common';
// Mongoose
import { MongooseModule } from '@nestjs/mongoose';
import { CryptoSchema } from './schemas/cryptoSchema';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [MongooseModule.forFeature([{name: 'Product', schema: CryptoSchema}]),
    CacheModule.register({
      store: redisStore,
      host: 'localhost',
      port: 6379,
    }),
    ScheduleModule.forRoot(),
    CryptoModule,
  ],
  providers: [CryptoService],
  controllers: [CryptoController],
})
export class CryptoModule {}
