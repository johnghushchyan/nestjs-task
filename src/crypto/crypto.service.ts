import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';

import { Model } from 'mongoose';
import { redisCache } from 'cache-manager';
import { InjectModel } from '@nestjs/mongoose';
import { Crypto } from './interfaces/product.interface';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class CryptoService {
    constructor(@InjectModel('Product') private readonly cryptoModel: Model<Crypto>, @Inject(CACHE_MANAGER) private cacheManager: redisCache) {
    }

    async createCrypto(createProductDTO: any): Promise<boolean> {
        const checkStore = await this.cacheManager.set('crypto', createProductDTO);
        return checkStore === 'OK';
    }

    @Cron('5 * * * * *')
    async handleCron() {
        const getStore = await this.cacheManager.get('crypto');
        const newCrypto = new this.cryptoModel(getStore);
        await newCrypto.save();
        await this.cacheManager.del('crypto');
        return;
    }
}
